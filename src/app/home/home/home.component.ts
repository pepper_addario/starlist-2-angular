import { Component, OnInit, Input } from '@angular/core';
// import { ICustomer } from '../../interfaces';
import { PersonagemService } from '../../personagem.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // @Input() customers: ICustomer[];
  customers;
  starshipInfo;

  constructor(
    private PersonagemService: PersonagemService
  ) { }

  loading = true;
  modalShow = false;
  paginate = 1;
  nextBtn = true;
  prevBtn = false;

  btnAction(starship) {
    this.loading = true;
    this.PersonagemService.getStarshipInfo(starship)
      .subscribe((data) => {
        this.starshipInfo = { ...data };
        this.modalShow = !this.modalShow;
        this.loading = false;
      });
  }

  hasData(data) {
    this.prevBtn = this.prevBtn = !!data['previous'];
    this.nextBtn = this.nextBtn = !!data['next'];
  }

  paginateNext() {
    this.paginate += 1;
    this.loading = true;
    this.nextBtn = !!this.paginate;
    this.PersonagemService.getPersonagemInfo(this.paginate)
      .subscribe((data) => {
        this.customers = data['results'];
        this.loading = false;
        this.hasData(data);
      });
  }

  paginatePrev() {
    this.paginate -= 1;
    this.loading = true;
    this.prevBtn = !!this.paginate;
    this.PersonagemService.getPersonagemInfo(this.paginate)
      .subscribe((data) => {
        this.customers = data['results'];
        this.loading = false;
        this.hasData(data);
      });
  }

  closeModal() {
    this.modalShow = false;
  }

  ngOnInit() {
    this.loading = true;
    this.PersonagemService.getPersonagemInfo()
      .subscribe((data) => {
        this.customers = data['results'];
        this.loading = false;
        if (this.paginate <= 1) { 
          this.prevBtn = false
        } 
      });

  }

}
