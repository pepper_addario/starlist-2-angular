import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PersonagemService {

  constructor(
    private http: HttpClient
  ) { }
  personagens: any = []; 


  getStarshipInfo(url) {
    return this.http.get(url);
  }

  getPersonagemInfo(paginate = 1) {
    // this.personagens = this.http.get('https://swapi.co/api/people/').subscribe(res=>this.personagens=res);
    // console.log(this.personagens + 'teste')
    return this.http.get(`https://swapi.co/api/people/?page=${paginate}`);
  }
}