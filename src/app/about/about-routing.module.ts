import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AboutComponent} from './about/about.component'
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


const routes: Routes = [{ 
  path: '', component: AboutComponent 
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutRoutingModule { }
