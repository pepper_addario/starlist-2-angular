import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { ICustomer } from './interfaces';

@Injectable({
  providedIn: 'root'
})

export class CharsListService {

  url = 'https://swapi.co/api/people/';

  constructor(private http: HttpClient) { }

  getChars() : Observable<ICustomer[]> {
    return this.http.get<ICustomer[]>(this.url)
               .map(data => {
                  return data['results'];
               });
  }
}