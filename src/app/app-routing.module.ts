import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


const routes: Routes = [
  { path: 'home', loadChildren: './home/home.module#HomeModule', data: {animation: 'HomePage'} },
  { path: 'about', loadChildren: './about/about.module#AboutModule', data: {animation: 'AboutPage'} }]

@NgModule({
  imports: [RouterModule.forRoot(routes), BrowserAnimationsModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
