import { Component, OnInit } from '@angular/core';
import { CharsListService } from './data-list.service'
import { trigger, state, style, animate, transition } from '@angular/animations';
import { RouterOutlet } from '@angular/router';
import { slideInAnimation } from './animations';
import { fadeAnimation } from './fade.animation'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    slideInAnimation,
    fadeAnimation
  ]
})
export class AppComponent implements OnInit {
  people: any[];
  loading: boolean = true;

  constructor(private CharsListService: CharsListService) { }

  ngOnInit() {
    this.CharsListService.getChars()
        .subscribe(data => {
          this.people = data;
          this.loading = false;
        });
  }
  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
  //public getRouterOutletState(outlet) {
  //  return outlet.isActivated ? outlet.activatedRoute : '';
  // }
}
